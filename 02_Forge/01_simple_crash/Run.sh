#!/bin/bash
set -x

make clean all
mpirun -np 4 ./mmult1_f90.exe

make clean all DEBUG=1
ddt --connect -np 4 ./mmult1_f90.exe

