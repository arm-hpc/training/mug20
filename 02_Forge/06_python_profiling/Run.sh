#!/bin/bash
set -x

mpirun -np 8 python ./diffusion-fv-2d.py

map --profile -np 8 python ./diffusion-fv-2d.py


