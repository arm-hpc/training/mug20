#!/bin/bash
set -x

make clean all
 
ddt --offline --output=initial.txt --mem-debug -np 4 ./*_c.exe
