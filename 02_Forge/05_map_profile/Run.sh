#!/bin/bash
set -x

make clean all
 
map --profile -np 4 ./*_c.exe 3096

map --export=profile.json *.map
