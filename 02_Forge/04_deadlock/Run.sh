#!/bin/bash
set -x

# Need 8 MPI procs for this example
export MV2_ENABLE_AFFINITY=0

make clean; make

mpirun -np 4 ./cpi.exe

mpirun -np 8 ./cpi.exe
 
ddt --connect -np 8 ./cpi.exe

