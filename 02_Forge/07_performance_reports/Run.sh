#!/bin/bash
set -x

make clean all
 
perf-report --output=result.html -np 4 ./*_c.exe 3096

perf-report --output=result.txt -np 4 ./*_c.exe 3096

